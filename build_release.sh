#!/bin/bash

# shellcheck source=/dev/null
source /home/ko/.bashrc

cat > ~/.transifexrc <<EOF
[https://www.transifex.com]
hostname = https://www.transifex.com
password = ${TRANSIFEX_TOKEN}
username = api
EOF

# exit immediately if any error happens
set -eo pipefail

echo "Cloning KOReader repository..."
rm -rf koreader
git clone https://github.com/koreader/koreader.git
pushd koreader

# Store the full log on disk to have access to it as an artifact
# NOTE: Alternatively, don't use tee here, just send the output to the log, and uncomment the tail in the CI's after_script
BUILD_LOG="build.log"
rm -f "${BUILD_LOG}"

git checkout "$(cat ../commit)"
echo "Building release..."
./kodev release "$1" 2>&1 | tee -a "${BUILD_LOG}"

echo "KOREADER_VERSION=$(git describe HEAD)">> "$GITLAB_ENV"
popd
